#!/usr/local/bin/python3

from sys import *
import cgi, cgitb

path.extend(['../conf/','../inc/'])
from zabbix_utils import *
from zabbix_conf import *

cgitb.enable()

form = cgi.FieldStorage()

htmlHeader="Content-Type: text/html\n\n"
newSearch ="<form action='device_check.py'><input type='submit' value='New search'></form>"


def main():

	search = '''
 			<form action="device_check.py">
				Device:<br>
 				<input type="text" name="hostname" maxlength="100">
 			 	<br><br>
				<input type="submit" value="Check">
			</form>
		'''
	
	try:
		hostname = form['hostname'].value.strip()
		hosts = zhostSearch(hostname)
	
		# If > 1 hosts, print host menu
		if len(hosts) > 1:
			print(htmlHeader)
			print('<html>')
			print("<form action='device_print.py'><select name='hostid'>")
			for host in hosts:
				print("<option value='"+host[1]+"'>"+host[0]+"</option>")
			print("</select>&nbsp<input type='submit' value='check'></form>")	
		
		#Immediately perform host check if only 1 match found
		elif len(hosts) == 1:
			
			print("Location: ./device_print.py?hostid="+hosts[0][1]+"\r\n")
			print("Connection: close \r \n")
			print("")
		
		#Host not configured in Zabbix
		else:
			print(htmlHeader)
			print('<html>')
			print('<tr><td>Host not found in Zabbix</td></tr>')				
			print('<tr><td><br></td></tr>')
			print('<tr><td>',newSearch,'</td></tr>')

	#Print search form if no 'hostname' arg
	except:
		print(htmlHeader)
		print('<html>')
		print(search)
	
	print("</html>")


main()
