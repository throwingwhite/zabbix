#!/usr/local/bin/python3

from sys import *
from pysnmp.entity.rfc3413.oneliner import cmdgen
from zabbix_api import ZabbixAPI
from glob import glob
import json, cgi, cgitb,re,urllib,threading,pymysql,socket

path.extend(['../conf/','../inc/'])
from zabbix_utils import *
from zabbix_conf import *

cgitb.enable()

form = cgi.FieldStorage()

newSearch ="<form action='device_check.py'><input type='submit' value='New search'></form>"


#Threading funtctions


# Check SNMP access (and routing) from each Zabbix Proxy
# Address: proxy host (to run query from)
# Host: target host
# Community: target host's community string

def zproxyCheck(address,host,community):
        result = extSnmpCheck(address,host,community)
	
        if not result:
                print('<tr><td>SNMP Check:',address,'</td><td><font color="#008800"> Ok </font></td></tr>')
        else:
                print('<tr><td>SNMP Check:',address,'</td><td><font color="#FF0000">',result,'</font></td></tr>')


# Check snmp access via proxy host
# Address: proxy host
# Host: target host
# Community: target host's commnity 

def publicSnmpCheck(address,host,community):
	result = extSnmpCheck(address,host,community)

	if not result:
		print('<tr><td>Non-management SNMP Check</td><td><font color="#FF0000"> Error: SNMP is responding to non-manamgenent hosts </font></td></tr>')
	else:
		print('<tr><td>Non-management SNMP Check</td><td><font color="008800"> No response (Ok)</font></td></tr>')


# Check port status via proxy host
# Address: proxy host
# Host: target host
# Port: target host port

def publicPortCheck(address,host,port):

	response = extPortCheck(address,host,port)

	print('<tr><td>Port check from non-management Address (TCP '+str(port)+'):</td>')

	if response:
		print('<td><font color="#FF0000">Open to non-management addresses</font></td></tr>')
	else:
		print('<td><font color="#008800">Closed (Ok)</font></td></tr>')

# Print host information
# hostid: Zabbix host ID
def printHostInfo(hostid):

	zhost = ZabbixHost(hostid)
	
	backup_template=False		# Backup template flag
	zyxel = False			# Zyxel flag

	#Check for Zyxel or Backup IP template and set flags
	for template in zhost.templates:
		if template['name'] == 'Template hSo Backup IP Check':
			backup_template=True
		if template['name'].find('Zyxel') > -1:
			zyxel = True
		
	print('<table cellspacing="10">')
	print('<tr><td>',newSearch,'</td></tr>')

	#Print Zabbix Host details
	print('<tr><td>Zabbix Host Name</td><td>',zhost.name,'</td></tr>')
	print('<tr><td>Zabbix Host Address</td><td>',zhost.address,'</td></tr>')

	#Check if multiple Zabbix hosts are configured with the same IP
	print('<tr><td>Duplicate IP check</td>')
	if len(zhost.dupSearch) > 1:
		dups = ' '.join(zhost.dupSearch)
		print('<td><font color="#FF0000">Error: devices are configured with the same IP ('
			+zhost.address+'): ',dups,'</font></td></tr>')
	else:
		print('<td><font color="008800">Ok</font></td></tr>')

	# Check forward DNS
	fqdn = zhost.dns or zhost.name+'.hso-group.net'
	print('<tr><td>DNS Check ('+fqdn+')</td>')
	try:
		raddress = str(socket.gethostbyname(fqdn))
		if raddress != zhost.address:
			print('<td><font color="#FF0000">The resolved address ('+raddress+') does not match the Zabbix'							,'host address ('+zhost.address+')</font></td></tr>')
		else:
			print('<td><font color="008800">',raddress,'</font></td></tr>')	
	except:		
		print('<td><font color="#FF0000"> Cannot resolve hostname</font></td></tr>')
			
	# Check for pointer record			
	print('<tr><td>Reverse DNS Check</td>')
	try:
		rdns = socket.gethostbyaddr(zhost.address)
		print('<td><font color="#008800">',rdns[0],'</font></td></tr>')
	except:			
		print('<td>Could not resolve pointer record </td></tr>')

	
	# If Backup IP template present, check if backup and primary IP are identical
	if zhost.backup and backup_template:
		print('<tr><td>Zabbix Backup Address: </td><td>')
		if zhost.backup == zhost.address:
			print('<font color=#FF0000>',zhost.backup,'is the same as the primrary IP</font>')
		else:
			print(zhost.backup)
		print('</td></tr>')

	# Check if backup IP is present but no backup template applied to host
	elif zhost.backup and not backup_template:
		print('<tr><td>Zabbix Backup Address: </td><td>')
		print('<font color=#FF0000>''Backup IP ('+zhost.backup+') is defined but no backup template is present </font>')
		print('</td></tr>')

	# Check if backup template is applied but no backup IP defined
	elif not zhost.backup and backup_template:
		print('<tr><td>Zabbix Backup Address: </td><td>')
		print('<font color=#FF0000> Backup template is present but no backup IP is defined </font>')
		print('</td></tr>')
				
	#Check Zabbix SNMP connectivity			
	print('<tr><td>Zabbix SNMP Connection Status:</td>')
	if zhost.snmperror:
		print('<td><font color="#FF0000">',zhost.snmperror,'</font></td>')
	elif not zhost.snmpavailable:
		print('<td>Unkown</td></tr>')
	else:
		print('<td><font color="#008800"> Ok </font></td></tr>')
	
	# List all templates applied to host
	print('<tr><td>Zabbix Templates:</td><td>')
	if zhost.templates:
		for template in zhost.templates:
			print(template['name'], end='<br>')
	else:
		print('<font color="FF000">No templates added to host</font>')
	print('</td></tr>')

	# List all triggers currently active for the host
	print('<tr><td>Zabbix Alarms:</td><td>')
	if zhost.triggers:
		#description, priority 1 -5 information,warn,avergae,high,disaster
		for trigger in zhost.triggers:
			print('<font color="'+severityColours[int(trigger["priority"])]
			+'">',trigger['description'])
			print(' ('+severity[int(trigger["priority"])]+')</font><br>')
		print('</td></tr>')
	else:
		print('<font color="#008800">None </font></td></tr>')

	print('<tr><td><br></td></tr>')

	# Check if device present in Rancid	
	if not zyxel:
		ranciddb = rancid_check(zhost.name)
		print('<tr><td>Rancid check</td>')
		if ranciddb['count'] == 1:
			print('<td><font color="#008800">Device found in',ranciddb['matches'][0][0],'</font></td>')
		
		# Alert if multiple instances of device configured in Rancid
		elif ranciddb['count'] > 1:
			for rancid_entry in ranciddb['matches']:
				print('<td><font color="#FF0000">',rancid_entry[1],'instances of device found in ',rancid_entry[0],'</td>')
			print('</tr>')
		else:
			print('<td><font color="#FF0000">Device not found in Rancid</font></td></tr>')
	
	# Query Portal database for customer account associated with device		
	dbh = pymysql.connect(host=dbhost ,user=dbuser ,password=dbpass ,db=dbname)
	cur = dbh.cursor()
	cur.execute("SELECT company,username from zabbix_link, users where zbx_link_zbx = "+zhost.hostid+" AND zbx_link_user = id")
	customer = cur.fetchone()
	print('<tr><td>hSo Portal Account: </td>')
	if customer:
		print('<td>',customer[0],' ('+customer[1]+')</td>')
	else:
		print('<td><font color="FF0000">Device not associated with a Portal account</font></td>')
	cur.close()
	dbh.close()

	mgmt_error = False	#Login success flag

	# Check HTTP if Zyxel
	if zyxel:
		print('<tr><td>Management Check (HTTP/HTTPS)</td>')
		try:
			# Load page and search for 'login' string
			content = httpCheck(zhost.address)
			if content.find('login') < 0:
				mgmt_error = 'Cannot load login page'
		except Exception as error:
			mgmt_error = error.reason
	else:		
		print('<tr><td>Management Check (SSH/Telnet)</td>')
		if ranciddb['count']:
			mgmt = MgDevice(zhost.name,ranciddb['type'])
			mgmt_error = mgmt.error
		else:
			mgmt_error = 'Device must be added to rancid to perform management check'
			
	if mgmt_error:
		print('<td><font color="#FF0000">',mgmt_error,'</font></td></tr>')
	else:
		print('<td><font color="#008800"> Ok </font></td></tr>')
		manage = True

	if not zyxel:
		pass

	#Check if syslog host configured and can be pinged. Skip Zyxel
	if not mgmt_error and not zyxel and ranciddb['type'] != 'foundry':
		print('<tr><td>Graylog Check ('+syslogip+'):</td>')
		if mgmt.syslogCheck():
			print('<td><font color="#008800">Syslog host configured</font></td></tr>')
		else:
			print('<td><font color="#FF0000">Syslog host not configured</font></td></tr>')

		print('<tr><td>Graylog Ping ('+syslogip+'):</td>')
		if mgmt.syslogPing():
			print('<td><font color="#008800">Ok</font></td></tr>')
		else:
			print('<td><font color="#FF0000">Ping failed</font></td></tr>')

	print('<tr><td><br></td></tr>')

	snmp = SnmpHost(zhost.address,zhost.community)

	if not snmp.error:
		print('<tr><td>SNMP Hostname</td><td>',snmp.hostname,'</td></tr>')
		if not zyxel:
			print('<tr><td>SNMP Location</td><td>',snmp.location,'</td></tr>')
	else:
	
		print('<tr><td>SNMP Device Check</td><td><font color="#FF0000">',snmp.error,'</font></td>')

	
	threads = []
	
	for zproxy in zproxies:
		t = threading.Thread(target=zproxyCheck, args=(zproxy,zhost.address,zhost.community))
		threads.append(t)
		t.start()
	if not zyxel:
		t1 = threading.Thread(target=publicSnmpCheck, args=(smokeping,zhost.address,zhost.community))
		threads.append(t1)
		t1.start()
	
		for port in [ 22, 23]:
			t = threading.Thread(target=publicPortCheck, args=(smokeping,zhost.address,port))
			threads.append(t)
			t.start()
				
def main():

	print("Content-Type: text/html\n\n")	
	print("<html>")
	
	try:
		hostid = form['hostid'].value.strip()

		printHostInfo(hostid)

	except:
		raise

	print("</html>")

main()
