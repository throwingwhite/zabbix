#!/usr/bin/env python3

from zabbix_api import ZabbixAPI
from sys import *
from pysnmp.entity.rfc3413.oneliner import cmdgen
from glob import glob
import re,pexpect,os,json,ssl
import urllib.request
import urllib.parse

path.append('../conf/')
from zabbix_conf import *

def extSnmpCheck(address,host,community):
        ''' Run external SNMP check from a non-management prefix '''
	defaul_context = ssl._create_default_https_context
	ssl._create_default_https_context = ssl._create_unverified_context

	response = urllib.request.urlopen('https://'+address+'/python/snmpcheck.py?host='+host+'&community='+community).read().strip()
	response = str(response,'utf-8')
	
	if response == 'success':        
		return 0
	else:
		return response

#External port check
def extPortCheck(address,host,port):
        ''' run external port check from a non-management host '''
	defaul_context = ssl._create_default_https_context
	ssl._create_default_https_context = ssl._create_unverified_context


	post = urllib.parse.urlencode({ 'host' : host,
                                	'port' : int(port)             })
	post = post.encode('UTF-8')

	response = urllib.request.urlopen('https://'+address+'/python/port_check.py', post)
	return int(response.read().decode('utf8','ignore').strip())


def zhostSearch(host):
        ''' Search Zabbix hosts using a pattern and return list of matches '''
	zapi = ZabbixAPI(server=zserver)
	zapi.login(auser,apass)
	hosts = zapi.host.get({'search' : { "host" : host }})
	matches = []
	if hosts:
		for host in hosts:
			matches.append((host['name'],host['hostid']))
	return(matches)

class ZabbixHost:

	zapi = ZabbixAPI(server=zserver)
	zapi.login(auser, apass)

	def __init__(self,hostid):
		self.host = self.zapi.host.get({ "filter": { "hostid" : hostid }, 
			"selectMacros" : "extend", 'selectParentTemplates' : 'extend', 'selectInterfaces' : 'extend'})
		
		if self.host:
			self.host = self.host[0]
			self.name = self.host['host']
			self.hostid = self.host['hostid']
			self.address = self.get_int_address()
			self.dns = self.get_dns()
			self.templateids = self.get_template_ids()
			self.templates = self.get_templates()
			self.community = self.get_community()
			self.triggers = self.get_triggers()
			self.snmperror = self.host['snmp_error']
			self.snmpavailable = int(self.host['snmp_available'])
			self.backup = self.get_backup_ip()
			self.dupSearch = self.dupSearch()

	def get_int_address(self):
                ''' get SNMP interface IP address or hostname of device '''
		interface = ''
		interfaces = self.host['interfaces']
		
		for interface in interfaces:
			if interface['type'] == '2':
				return interface['ip'] or interface['dns']
	
	def get_dns(self):
                ''' Get hostname for SNMP interface of host '''
		interface = ''
		interfaces = self.host['interfaces']
		
		for interface in interfaces:
			if interface['type'] == '2':
				return interface['dns']

	def get_template_ids(self):
                ''' obtain and return a list of template IDs of templates applied to host'''
		templates = self.host['parentTemplates']
		templateids = []
		for template in templates:
			templateids.append(template['templateid'])
		return templateids

	def get_templates(self):
                ''' return a list of names of templates that have been applied to the host '''
		return self.zapi.template.get({ 'templateids' : self.templateids, 'selectMacros' : 'extend'} )
	
	def get_backup_ip(self):
                ''' return value of the BACKUP_IP macro if it exists '''
		backup_ip = ''

		if self.host['macros']:
			for macro in self.host['macros']:
				if macro['macro'] == '{$BACKUP_IP}':
					backup_ip = macro['value']
		return backup_ip
			

	def get_community(self):
                ''' return value of the SNMP_COMMUNITY macro if exists '''
		snmp_community = 'public'

		if self.host['macros']:
			for macro in self.host['macros']:
				if macro['macro'] == '{$SNMP_COMMUNITY}':
					snmp_community = macro['value']
		if not snmp_community:
			for template in self.templates:
				if template['macros']:
					for macro in template['macros']:
						if macro['macro'] == '{$SNMP_COMMUNITY}':
							snmp_community = macro['value']

		return snmp_community


	def get_triggers(self):
                ''' get a list of currently active triggers for the host (value = 1) '''
		triggers = self.zapi.trigger.get({ "hostids" : self.hostid, 
			"filter" : { "value" : 1 } , "expandDescription" : "extend" })

		trigger_desc = []

		for trigger in triggers:
			trigger_desc.append(trigger['description'])

		return triggers

	def dupSearch(self):
                ''' return a list of any hosts (duplicates) configured with the same interface IP '''
		duplicates = self.zapi.hostinterface.get({ "filter": { "ip" : self.address }})
		dupList = []
		host_compare = []
		if len(duplicates) > 1:
			for duplicate in duplicates:
				host = self.zapi.host.get({ "filter": { "hostid" : duplicate['hostid'] }})
				host_compare.append(int(duplicate['hostid']))
				dupList.append(host[0]['host'])
		
			if sum(host_compare) / len(host_compare) == host_compare[0]:
				dupList = []

		return dupList




class SnmpHost:
        ''' check SNMP connectivity andreturn snmp location and hostname values '''
	def __init__(self, host, community):
		
		cmdGen = cmdgen.CommandGenerator()

		self.error = 0	

		errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
		    cmdgen.CommunityData(community),
	   	 cmdgen.UdpTransportTarget((host, 161)),
	   	 '1.3.6.1.2.1.1.5.0',
	   	 '1.3.6.1.2.1.1.6.0'  
		)
	
		# Check for errors and print out results
		if errorIndication:
		    self.error = errorIndication
		else:
		    if errorStatus:
		        self.erro = errorStatus
		        print('%s at %s' % (
		            errorStatus.prettyPrint(),
		            errorIndex and varBinds[int(errorIndex)-1] or '?'
		            )
		        )
		    else:
		        self.hostname = varBinds[0][1]
		        self.location = varBinds[1][1]

def rancid_check(hostname):
        ''' check that the host is configured in a rancindb file '''
	rancidMatch = {}
	rancidMatch['count'] = 0
	rancidMatch['matches'] = []
	rancidMatch['type'] = ''

	routerdbs = glob(rancid_path+'var/*/router.db')
        ''' search all rancidb files for the zabbix hosts'''
	for routerdb in routerdbs:
		rdb = open(routerdb)
		match = re.findall(hostname+'[^\n]*',rdb.read())
		if match:
			match2 = []
			for entry in match:
				if entry.find(':') == -1:
					match2.append(entry)
				
			matches = len(match2)
			rancidMatch['count'] += matches
			if matches:
				rancidMatch['matches'].append((routerdb, matches))
				if not rancidMatch['type']:
					rancidMatch['type'] = match2[0].split(';')[1]
		rdb.close()
	return rancidMatch  # return the number of matches

def httpCheck(hostname):
        ''' check https access to host '''

	defaul_context = ssl._create_default_https_context	
	ssl._create_default_https_context = ssl._create_unverified_context

	#print(hostname)
	
	request = urllib.request.urlopen('http://'+hostname,None,5)
	return str(request.read(),'utf-8')



class MgDevice:

	def __init__(self,host,dtype,connect_timeout=5,command_timeout=5):
                ''' Login to device using pxpect and racnid expect script '''
		self.dtype = dtype
		self.eofcmd = '\nEOF'
		self.eof = '.*'+device_unknown_cmd[self.dtype]		

		self.console = pexpect.spawn('/usr/bin/sudo -u rancid /usr/libexec/rancid/'+rancid_devices[dtype][0]+' -t ' + str(connect_timeout) + ' ' + host,searchwindowsize=30000,maxread=30000)
		result = self.console.expect([rancid_devices[dtype][1],'Error:.*'])

		if result:
			self.error=self.console.after.decode().rstrip()
		else:
			self.error=None

	def syslogCheck(self):
                ''' check remote logging is configured '''
		self.console.sendline(syslog_config_cmd[self.dtype])
		self.console.sendline(self.eofcmd)
		try:
			self.console.expect(self.eof,timeout=10)
			return re.search(syslogip,self.console.after.decode())
		except:
			return None

	
	def syslogPing(self):
                '''  check ICMP connecitivity to remote logging host '''
		self.console.sendline(syslog_ping_cmd[self.dtype]+self.eofcmd)
		self.console.sendline(self.eofcmd)
		try:
			self.console.expect(self.eof,timeout=10)
			return re.search(syslog_ping_check[self.dtype],self.console.after.decode()) 
			
		except:
			raise
			return None

