dbuser="dbuser"
dbpass="dbpassword"
dbname="customerdb"
dbhost="8.8.8.8"

auser="devuser"
apass="devpassword"

zserver="https://zabbix.example.com"

syslogip="9.9.9.9"

smokeping="6.6.6.6" # external host on non-management prefix

zproxies = [    'zproxy01.example.com',
                'zproxy02.example.com',
                'zproxy03.example.com',
                'zproxy04.example.com' ]

severity = {    1 : 'Information',
                2 : 'Warning',
                3 : 'Average',
                4 : 'High',
                5 : 'Disaster' }

severityColours = {     1 : '#D6F6FF',
                        2 : '#FFF6A5',
                        3 : '#FFB689',
                        4 : '#FF9999',
                        5 : '#FF3838' }


rancid_path="/usr/local/rancid/"

rancid_devices = {'cisco' : ['clogin','\n[^\n]*#'],
		'foundry' : ['flogin','\n[^\n]*#'],
		'juniper' : ['jlogin','\n[^\n]*>'],
		'netscreen' : ['nlogin','\n[^\n]*->'],
		'riverstone' : ['rivlogin','\n[^\n]*#']}

syslog_ping_cmd = {	'cisco' : 'ping '+syslogip+' repeat 1',
			'foundry' : 'ping '+syslogip+' count 1',
			'juniper' : 'ping '+syslogip+' count 1',
			'netscreen' : 'ping '+syslogip+' count 1']}

syslog_ping_check = {	'cisco' : 's rate is 100 percent',
			'foundry' : 's rate is 100 percent',
			'juniper' : '1 packets received',
			'netscreen' : 's Rate is 100 percent'}

syslog_config_cmd = {	'cisco' : 'show run | in logging',
			'foundry' : 'show config | in logg',
			'juniper' : 'show configuration system syslog',
			'netscreen' : 'get config | in "syslog config' }

device_unknown_cmd = {	'cisco' : '#\r\n',
			'foundry' : 'Type ? for',
			'juniper' : 'unknown command',
			'netscreen' : 'unknown keyword' }
