#!/usr/bin/perl
# required since '$' symbol cannot be passed to a Zabbix external script 

package zvars;

use strict;
use warnings;

#Global Variables
our $jsonHead = "{\n\t\"data\":[\n\n";
our $jsonTail = "\n\t]\n}\n";

our $communities = {    'e3xam'=>'e3xample$community',    #NetScreen
                        'e2xam'=>'e2xample$community' }      #Cisco 
